<?php

class Template
{
    public $id;
    public $subject;
    public $content;

    public function __construct($id, $subject, $content)
    {
        $this->id = $id;
        $this->subject = $subject;
        $this->content = $content;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function compileSubject($values)
    {
        $this->subject = $this->compile($this->subject, $values);
        return $this;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function compileContent($values)
    {
        $this->content = $this->compile($this->content, $values);
        return $this;
    }

    /**
     * @param string $template
     * @param array $values
     * @return string
     */
    private function compile($template, $values)
    {
        return str_replace(array_keys($values), array_values($values), $template);
    }
}