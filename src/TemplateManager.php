<?php

class TemplateManager
{
    private $placeHolders = [
        '[quote:destination_link]' => '',
        '[quote:summary_html]' => '',
        '[quote:summary]' => '',
        '[quote:destination_name]' => '',
        '[user:first_name]' => ''
    ];

    public function getTemplateComputed(Template $tpl, array $data)
    {
        if (!$tpl) {
            throw new \RuntimeException('no tpl given');
        }

        $replaced = clone($tpl);

        $values = $this->computeValues($data, $this->placeHolders);

        $replaced->compileSubject($values)
            ->compileContent($values);

        return $replaced;
    }

    private function computeValues(array $data, $placeholders)
    {
        $applicationContext = ApplicationContext::getInstance();

        $quote = (isset($data['quote']) and $data['quote'] instanceof Quote) ? $data['quote'] : null;

        if ($quote) {

            $quoteFromRepository = QuoteRepository::getInstance()->getById($quote->id);
            $quoteSite = SiteRepository::getInstance()->getById($quote->siteId);
            $quoteDestination = DestinationRepository::getInstance()->getById($quote->destinationId);

            $placeholders['[quote:summary_html]'] = Quote::renderHtml($quoteFromRepository);
            $placeholders['[quote:quote:summary]'] = Quote::renderText($quoteFromRepository);
            $placeholders['[quote:destination_name]'] = $quoteDestination->countryName;
            $placeholders['[quote:destination_link]'] = sprintf('%s/%s/quote/%s', $quoteSite->url, $quoteDestination->countryName, $quoteFromRepository->id);
        }

        $user = (isset($data['user']) and $data['user'] instanceof User) ? $data['user'] : $applicationContext->getCurrentUser();
        if ($user) {
            $placeholders['[user:first_name]'] = ucfirst(mb_strtolower($user->firstname));
        }

        return $placeholders;

    }

}
