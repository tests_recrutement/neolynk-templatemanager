<?php

use Faker\Factory;

require_once __DIR__ . '/../src/Entity/Destination.php';
require_once __DIR__ . '/../src/Entity/Quote.php';
require_once __DIR__ . '/../src/Entity/Site.php';
require_once __DIR__ . '/../src/Entity/Template.php';
require_once __DIR__ . '/../src/Entity/User.php';
require_once __DIR__ . '/../src/Helper/SingletonTrait.php';
require_once __DIR__ . '/../src/Context/ApplicationContext.php';
require_once __DIR__ . '/../src/Repository/Repository.php';
require_once __DIR__ . '/../src/Repository/DestinationRepository.php';
require_once __DIR__ . '/../src/Repository/QuoteRepository.php';
require_once __DIR__ . '/../src/Repository/SiteRepository.php';
require_once __DIR__ . '/../src/TemplateManager.php';

class TemplateManagerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var Quote
     */
    private $quote;

    /**
     * @var Destination
     */
    private $user;

    /**
     * @var User
     */
    private $destination;

    /**
     * Init the mocks
     */
    public function setUp()
    {
        $faker = Factory::create();
        $this->quote = new Quote($faker->randomNumber(), $faker->randomNumber(), $faker->randomNumber(), $faker->date());
        $this->destination = DestinationRepository::getInstance()->getById($faker->randomNumber());
        $this->user = ApplicationContext::getInstance()->getCurrentUser();
    }

    /**
     * Closes the mocks
     */
    public function tearDown()
    {
        $this->quote = null;
        $this->user = null;
        $this->destination = null;
    }

    /**
     * Successful Test 1
     * @test
     */
    public function test()
    {
        $template = new Template(
            1,
            'Votre voyage avec une agence locale [quote:destination_name]',
            "
Bonjour [user:first_name],

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
");
        $templateManager = new TemplateManager();

        $message = $templateManager->getTemplateComputed(
            $template,
            [
                'quote' => $this->quote
            ]
        );
        $this->assertEquals($template->id, $message->id);
        $this->assertEquals('Votre voyage avec une agence locale ' . $this->destination->countryName, $message->subject);
        $this->assertEquals("
Bonjour " . $this->user->firstname . ",

Merci d'avoir contacté un agent local pour votre voyage " . $this->destination->countryName . ".

Bien cordialement,

L'équipe Evaneos.com
www.evaneos.com
", $message->content);
    }

    /**
     * @test
     */
    public function testException()
    {
        $templateManager = new TemplateManager();

        if (PHP_VERSION > '7') {
            $this->expectException(\TypeError::class);
        } else {
            $this->expectException(\RuntimeException::class);
        }

        $templateManager->getTemplateComputed("Not a template object", []);
    }

    /**
     * The placeholder quote:destination_naame doesn't exists, it should not be replaced with anything
     * @test
     */
    public function testUnexistedPlaceholder()
    {
        $template = new Template(
            1,
            'Votre voyage avec une agence locale [quote:destination_naame]',
            "
Bonjour [user:first_name],

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].
");
        $templateManager = new TemplateManager();

        $message = $templateManager->getTemplateComputed(
            $template,
            [
                'quote' => $this->quote
            ]
        );
        $this->assertEquals($template->id, $message->id);
        $this->assertEquals('Votre voyage avec une agence locale [quote:destination_naame]', $message->subject);
        $this->assertEquals("
Bonjour " . $this->user->firstname . ",

Merci d'avoir contacté un agent local pour votre voyage " . $this->destination->countryName . ".
", $message->content);
    }

    public function testWithUser()
    {
        $user = new User(20, 'John', 'DOE', 'john.doe@anonymous.net');

        $template = new Template(
            1,
            'Votre voyage avec une agence locale [quote:destination_name]',
            "
Bonjour [user:first_name],

Merci d'avoir contacté un agent local pour votre voyage [quote:destination_name].

Bien cordialement,
");
        $templateManager = new TemplateManager();

        $message = $templateManager->getTemplateComputed(
            $template,
            [
                'quote' => $this->quote,
                'user' => $user
            ]
        );
        $this->assertEquals($template->id, $message->id);
        $this->assertEquals('Votre voyage avec une agence locale ' . $this->destination->countryName, $message->subject);
        $this->assertEquals("
Bonjour " . $user->firstname . ",

Merci d'avoir contacté un agent local pour votre voyage " . $this->destination->countryName . ".

Bien cordialement,
", $message->content);
    }
}
