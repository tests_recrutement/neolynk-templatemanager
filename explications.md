## Explications:

### 1er commit:

Ajout d'autres tests unitaires pour éviter les régressions et aussi avoir plus de couverture de code

### 2éme commit:

La classe `Template` sera responsable de remplacer les placeholders par leurs valeurs respectives.
Il doit juste recevoir la valeur de chaque placeholder

### 3éme commit:

La classe `TemplateManager` sera responsable de calcul des valeurs des placeholders.
Une propriété private `$placeholders` est ajoutée pour initier les valeurs des placeholders supportés.
